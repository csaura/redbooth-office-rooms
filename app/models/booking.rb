class Booking < ActiveRecord::Base
  belongs_to :room

  validates_datetime :booked_at
end
