class Room < ActiveRecord::Base
  has_many :bookings

  validates :name, presence: true
  validates :description, length: { minimum: 20}

  before_save :add_emoji_to_description

  private
  def add_emoji_to_description
    description << ' :trollface: '
  end
end
