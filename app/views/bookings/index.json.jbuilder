json.array!(@bookings) do |booking|
  json.extract! booking, :id, :booked_by, :room_id, :booked_at, :minutes
  json.url booking_url(booking, format: :json)
end
