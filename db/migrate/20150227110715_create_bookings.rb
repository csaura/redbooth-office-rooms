class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.string :booked_by
      t.references :room, index: true
      t.datetime :booked_at
      t.integer :minutes

      t.timestamps null: false
    end
    add_foreign_key :bookings, :rooms
  end
end
